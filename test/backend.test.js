const { expect } = require('chai')
const request = require('supertest')

// App
const app = require('../app')
const agent = request.agent(app)

// Test suite
describe('API', () => {
  it('should say `Hello Hackathon`', async () => {
    const res = await request(app).get('/api')
    expect(res.status).to.equal(200)
    expect(res.text).to.be.a('string')
    expect(res.text).to.equal('Hello Hackathon')
  })

  it('should echo a query', done => {
    agent
      .get('/api')
      .query({ q: 'echo' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('echo')
        done()
      })
  })

  it('should calulate two numbers', done => {
    agent
      .get('/api')
      .query({ q: '5ff6aad0: what is 0 plus 14' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('14')
        done()
      })
  })

  it('should minus two numbers', done => {
    agent
      .get('/api')
      .query({ q: '5ff6aad0: what is 10 minus 4' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('6')
        done()
      })
  })

  it('should give max number', done => {
    agent
      .get('/api')
      .query({ q: '71df5490: which of the following numbers is the largest: 73, 497' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('497')
        done()
      })
  })

  it('should multiply a by b', done => {
    agent
      .get('/api')
      .query({ q: 'a170bf20: what is 9 multiplied by 10' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('90')
        done()
      })
  })

  it('should give both a square and cubes', done => {
    agent
      .get('/api')
      .query({ q: 'd86d5760: which of the following numbers is both a square and a cube: 447, 400, 706, 1521' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('400')
        done()
      })
  })

  it('should print primes', done => {
    agent
      .get('/api')
      .query({ q: '07dbb2f0: which of the following numbers are primes: 3, 5, 7, 9, 11' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('3, 5, 7, 11')
        done()
      })
  })

  it('should give power of number', done => {
    agent
      .get('/api')
      .query({ q: '07dbb2f0: what is 2 to the power of 3' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('8')
        done()
      })
  })

  context('should error', () => {
    it('on request to a non-existing endpoint', done => {
      agent
        .get('/api/bla')
        .expect(404)
        .end(done)
    })
  })
})
