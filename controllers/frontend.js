module.exports = {
  home: (req, res) => {
    res.status(200).render('home', { company: 'Tom&Co' })
  }
}
