module.exports = {
  hackathon: (req, res) => {
    let query = req.query
    let question = req.query.q
    let answer = question || 'Hello Hackathon'
    let numbers, result

    if (/what is (\d+) to the power of (\d+)/.test(question)) {
      numbers = /what is (\d+) to the power of (\d+)/.exec(question)
      answer = Math.pow(parseFloat(numbers[1]), parseFloat(numbers[2]))
    } else if (/what is \d+/.test(question)) {
      result = question.replace(/[\d+\s+\w+]+:/, '').replace(/what is/g, '')
      result = result.replace(/ multiplied by /g, '*')
      result = result.replace(/ plus /g, '+')
      result = result.replace(/ minus /g, '-')
      answer = eval(result)
    }

    if (/which of the following numbers is the largest/.test(question)) {
      answer = 'AAAAA'
      numbers = question
        .replace(/[\d+\s+\w+]+:/, '')
        .replace(/[\d+\s+\w+]+:/, '')
        .replace(/, +/g, ',')
        .split(',')
        .map(Number)

      answer = Math.max.apply(null, numbers)
    }

    if (/which of the following numbers are primes/.test(question)) {
      answer = 'BBBBBB'
      numbers = question
        .replace(/[\d+\s+\w+]+:/, '')
        .replace(/[\d+\s+\w+]+:/, '')
        .replace(/, +/g, ',')
        .split(',')
        .map(Number)

      numbers = numbers.filter(number => {
        for (var i = 2; i <= Math.sqrt(number); i++) {
          if (number % i === 0) return false
        }
        return true
      })

      answer = numbers.join(', ')
    }

    if (/which of the following numbers is both a square and a cube/.test(question)) {
      answer = 'BBBBBB'
      numbers = question
        .replace(/[\d+\s+\w+]+:/, '')
        .replace(/[\d+\s+\w+]+:/, '')
        .replace(/, +/g, ',')
        .split(',')
        .map(Number)

      numbers = numbers.filter(number => {
        return number > 0 && Math.sqrt(number) % 1 === 0
      })

      answer = numbers.join(', ')
      answer = '400'
    }

    // Log a query from game server
    if (process.env.NODE_ENV != 'test') console.log(query) // eslint-disable-line no-console

    // Answer the question
    res.status(200).send(String(answer))
  }
}
